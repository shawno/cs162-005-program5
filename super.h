// This file will include my constants,
// any classes or structs I will be using,
// and all my #includes that will be needed.
// Shawn Odom, Program 5, 3/7/24

#include <cctype>
#include <cstring>
#include <iostream>
using namespace std;

//List of constants for arrays.
const int NAME = 51;
const int STORY = 201;
const int THEME = 151;
const int POWERS = 201;
const int COLOR = 26;

struct super
{
	char * super_name;
	char super_story[STORY];
	char super_theme[THEME];
	char super_powers[POWERS];
	char super_color[COLOR];
};

struct node
{
	super data;
	node * next;
};
