// This will will hold my main function and
// will call all the functions I make in super.cpp
// Shawn Odom, 3/7/24, Program 5
// The purpose of this program is to allow
// the user to enter in their favorite superheroes
// into a list, giving them the choice of how large
// they would like their list to be. Each superhero
// has its own list of information included with it
// that will be output when the final list is displayed. 
#include "super.h"

void read_in_super(super * super_array, int & num_supers, int list_size);
void add_to_linear_list(super * super_array, node * & head, int num_supers);
void append_to_linear_list(super * super_array, node * & head, int num_supers);
void sort_list(node * & head);
void display_all(node * & head);
void delete_list(node * & head);

int main()
{
	node * head = nullptr;
	int list_size = 0;
	int num_supers = 0;
	super * super_array;

	cout << "\nWelcome to my program! This program is designed to allow you"
		<< " to create a list of your favorite superheros!"
		<< "\nHow many superheros would you like to add?: ";
	cin >> list_size;
	cin.ignore(100, '\n');

	super_array = new super[list_size];
	read_in_super(super_array, num_supers, list_size);
//	add_to_linear_list(super_array, head, num_supers);
	append_to_linear_list(super_array, head, num_supers);
	sort_list(head);
	display_all(head);
	delete_list(head);

	if (num_supers > 0)
	{
		delete [] super_array;
	}

	return 0;
}





























































