// This file will hold my functions which will be called in main
// Shawn Odom, 3/7/24, Program 5
#include "super.h"

// Allow the user to enter all
// the important information regarding
// a superhero of their choosing
void read_in_super(super * super_array, int & num_supers, int list_size)
{
	
		char response = ' ';
		do
		{
			if (num_supers >= list_size)
			{
				cout << "\nThat's as many superheroes as you can add!" << endl;
				return;
			}

			char name[NAME];

			cout << "\nYou have decided to enter in a new superhero to your list! "
			<< "fill out the following information for me!" << endl;

			cout << "Enter your superheros name: ";
			cin.get(name, NAME, '\n');
			cin.ignore(100, '\n');
			super_array[num_supers].super_name = new char [strlen(name) + 1];
			strcpy(super_array[num_supers].super_name, name);

			cout << "Enter your superheros backstory: ";
			cin.get(super_array[num_supers].super_story, STORY, '\n');
			cin.ignore(100, '\n');

			cout << "Enter the superheros theme, brief description: ";
			cin.get(super_array[num_supers].super_theme, THEME, '\n');
			cin.ignore(100, '\n');

			cout << "Now please enter the superheros powers!: ";
			cin.get(super_array[num_supers].super_powers, POWERS, '\n');
			cin.ignore(100, '\n');

			cout << "Lastly, enter the color of their supersuit!: ";
			cin.get(super_array[num_supers].super_color, COLOR, '\n');
			cin.ignore(100,'\n');

			++num_supers;
			do
			{
				cout << "Would you like to add another? (y or n): ";
				cin >> response;
				cin.ignore(100, '\n');
				response = tolower(response);
			} while (response != 'y' && response != 'n');
		} while (response == 'y');
}
// Add the information the user input
// into the array into the front of the linear linked list
void add_to_linear_list(super * super_array, node * & head, int num_supers)
{
	for (int i = 0; i < num_supers; ++i)
	{
		node * newNode = new node;
		newNode ->data = super_array[i];
		newNode -> next = head;
		head = newNode;
	}
}
// Add the information from the array
// to the list at the end
void append_to_linear_list(super * super_array, node * & head, int num_supers)
{
	for (int i = 0; i < num_supers; ++ i)
	{
		node * newNode = new node;
		newNode -> data = super_array[i];
		newNode -> next = nullptr;

		if (!head)
		{
			head = newNode;
		}
		else
		{
			node * current = head;
			while (current -> next != nullptr)
			{
				current = current -> next;
			}
			current -> next = newNode;
		}
	}
}
// Sort the linear linked list by 
// the superheros first name
// alphbetically
void sort_list(node * & head)
{
	if (head == nullptr || head -> next == nullptr)
	{
		return; //This list cannot be sorted/is already sorted
	}

	node * sorted_list = nullptr;
	node * current = head;

	while (current != nullptr)
	{
		node * nextNode = current -> next; //Move current to next node
		
		node * temp = sorted_list; //Traversal Node
		node * prev = nullptr; //Previous pointer
		
		while (temp != nullptr && strcmp(current -> data.super_name, temp -> data.super_name) > 0)
		{
			prev = temp;
			temp = temp -> next;
		}
		//After we find the spot the data belongs
		//This first if statements will see if it belongs first in the list
		if (prev == nullptr) 
		{
			current -> next = sorted_list; //Point to the rest of the sorted list
			sorted_list = current; //Set the head of the sorted list to current
		}
		else
		{
			//All other cases
			prev -> next = current; //Set the node before where current needs to go to point to current
			current -> next = temp; //Set current to point to the next node in the sorted list
		}

		current = nextNode; //End by getting a new node
	}

	head = sorted_list; //Update head to the new lista
}
//Display the linear linked listed after it has
//been sorted, diplaying all the information the
//user input
void display_all(node * & head)
{
	node * current = head;
	while (current != nullptr)
	{
		cout << "\nSuperhero Name: " << current -> data.super_name << endl;
                cout << "\nSuperhero Backstory: " << current -> data.super_story << endl;
                cout << "\nSuperhero Theme: " << current -> data.super_theme << endl;
                cout << "\nSuperhero Power List: " << current -> data.super_powers << endl;
                cout << "\nSuperhero Suit Color: " << current -> data.super_color << endl;
		
		current = current -> next;
	}
}
//Deleting the dynamically allocated memeory
//insuring there is no memory leaks,
void delete_list(node * & head)
{
	while (head != nullptr)
	{
		node * temp = head;
		head = head -> next;

		delete [] temp -> data.super_name;
		temp -> data.super_name = nullptr;

		delete temp;
	}
}
